package team.gu.com.workingtimecontrol.security;

import io.paperdb.Paper;

//TODO пока нету сервера и организованной БД то храним юзера в базе тут

public class PaperBooksReaderWriter {
    public static final String PAPER_USERS = "cards_book";

    public static <T> boolean writeDataToBook(T object, String key, String bookName) {
        Paper.book(bookName).write(key, object);
        return true;
    }

    public static <T> T readDataFromBook(String bookName, String key) {
        T wasRead = Paper.book(bookName).read(key);
        return wasRead;
    }
}
