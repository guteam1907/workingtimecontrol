package team.gu.com.workingtimecontrol.security;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

import team.gu.com.workingtimecontrol.application.App;

/**
 * Класс по добавлению параметров приложения во внутреннее приватное хранилище приложения
 * в качестве ключа берется (androidId+savedParameterKey).hashCode, значение сохраняемого
 * параметра также хешиуется
 */
public class SharedPreferenceWorker implements SecurityDataSaver {
    ;
    private static final String TAG = "SHARED_WORKER ";
    private static final String KEY_AUTHORISED = String.valueOf("USER_WAS_AUTHORISED".hashCode());
    private static final String KEY_PIN_EXIST = String.valueOf("PINCODE_IS_EXIST".hashCode());
    private static final String KEY_LOGIN = String.valueOf("IT_IS_USERS_LOGIN".hashCode());
    private static final String KEY_CHIPER = String.valueOf("IT_IS_USERS_CHIPER".hashCode());
    private static final String PREFERENCE = String.valueOf("MY_SHARED_PREFERENCE".hashCode());
    private static final String KEY_WORKER = String.valueOf("WORKER".hashCode());
    private static final String KEY_LEADER = String.valueOf("LEADER".hashCode());
    private static SharedPreferenceWorker ourInstance;

    private static final String KEY_PASSWORD = String.valueOf("IT_IS_USERS_PASSWORD".hashCode());

    private SharedPreferenceWorker() {
    }

    public static SharedPreferenceWorker getInstance() {
        if (ourInstance == null) {
            ourInstance = new SharedPreferenceWorker();
        }
        return ourInstance;
    }

    /**
     * Проверка был ли авторизован пользователь
     */
    @Override
    public boolean checkAuthorised() {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        return sharedPreferences.getBoolean(String.valueOf((KEY_AUTHORISED + androidId).hashCode()), false);
    }

    /**
     * Сохранение статуса авторизации
     */
    @Override
    public void saveAuthState(boolean isAuthorised) {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        editor.putBoolean(String.valueOf((KEY_AUTHORISED + androidId).hashCode()), isAuthorised);
        editor.apply();
    }

    /**
     * Сохранение статуса ввода пинкода
     */
    @Override
    public void savePinCodeExistState(boolean isExist) {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        editor.putBoolean(String.valueOf((KEY_PIN_EXIST + androidId).hashCode()), isExist);
        editor.apply();
    }

    /**
     * Проверка установлен ли в приложении пинкод
     */
    @Override
    public boolean checkPinCodeExisting() {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        return sharedPreferences.getBoolean(String.valueOf((KEY_PIN_EXIST + androidId).hashCode()), false);
    }

    /**
     * Сохранение шифрованного пароля для входа в приложение
     */
    @Override
    public void saveChiper(String chiper) {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        editor.putString(String.valueOf((KEY_CHIPER + androidId).hashCode()), chiper);
        editor.apply();
    }

    /**
     * Получение логина юзера
     */
    @Override
    public String getUserChiper() {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        return sharedPreferences.getString(String.valueOf((KEY_CHIPER + androidId).hashCode()), "");
    }

    /**
     * Сохранение логина юзера
     */
    @Override
    public void saveUserLogin(String login) {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        editor.putString(String.valueOf((KEY_LOGIN + androidId).hashCode()), login);
        editor.apply();
    }

    /**
     * Получение логина юзера
     */
    @Override
    public String getUserLogin() {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        return sharedPreferences.getString(String.valueOf((KEY_LOGIN + androidId).hashCode()), "");
    }

    /**
     * Проверка работник ли юзер
     */
    @Override
    public boolean checkWorkerState() {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        return sharedPreferences.getBoolean(String.valueOf((KEY_WORKER + androidId).hashCode()), false);
    }

    /**
     * Сохранение статуса работника
     */
    @Override
    public void saveWorkerState(boolean isWorker) {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        editor.putBoolean(String.valueOf((KEY_WORKER + androidId).hashCode()), isWorker);
        editor.apply();
    }

    /**
     * Проверка лидер ли юзер
     */
    @Override
    public boolean checkLeaderState() {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        return sharedPreferences.getBoolean(String.valueOf((KEY_LEADER + androidId).hashCode()), false);
    }

    /**
     * Сохранение статуса лидера
     */
    @Override
    public void saveLeaderState(boolean isLeader) {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        editor.putBoolean(String.valueOf((KEY_LEADER + androidId).hashCode()), isLeader);
        editor.apply();
    }

    /**
     * Сохранение пароля юзера, временный метод, нужне только для авторизации, т.к. пароль должен храниться на сервере или в БД
     */
    @Override
    public void saveUserPassword(String password) {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        editor.putString(String.valueOf((KEY_PASSWORD + androidId).hashCode()), password);
        editor.apply();
    }

    /**
     * Получение пароля юзера, временный метод, нужне только для авторизации, т.к. пароль должен храниться на сервере или в БД
     */
    @Override
    public String getUserPassword() {
        Application app = App.getInstance();
        SharedPreferences sharedPreferences = app.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        String androidId = Settings.Secure.getString(app.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        return sharedPreferences.getString(String.valueOf((KEY_PASSWORD + androidId).hashCode()), "");
    }
    //TODO хранение пинкода в приложении
//    private static final String KEY_PIN = String.valueOf("IT_IS_USERS_PIN".hashCode());
//    /**
//     * Проверка правильности пинкода
//     */
//    public static boolean checkPinCode(Activity activity, String pincode) {
//        SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
//        String androidId = Settings.Secure.getString(activity.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//        int result = sharedPreferences.getInt(String.valueOf((KEY_PIN + androidId).hashCode()), -1);
//        Log.d(TAG, "checkPinCode: androidId = " + androidId + "\n"
//                + "Result = " + result + "\n" +
//                "PinCodeHash = " + pincode.hashCode());
//        return result == (pincode.hashCode());
//    }

//    /**
//     * Сохранение пинкода для входа в приложение
//     */
//    public static void savePinCode(Activity activity, String pincode) {
//        SharedPreferences sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        String androidId = Settings.Secure.getString(activity.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//        editor.putInt(String.valueOf((KEY_PIN + androidId).hashCode()), pincode.hashCode());
//        editor.apply();
//    }
}
