package team.gu.com.workingtimecontrol.regOrAuthModule.presenter;


import team.gu.com.workingtimecontrol.regOrAuthModule.model.ModelMain;
import team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface.ViewContainer;
import team.gu.com.workingtimecontrol.security.SecurityDataSaver;
import team.gu.com.workingtimecontrol.security.SharedPreferenceWorker;

public class PresenterMain {
    private ViewContainer view;
    private ModelMain model;
    private SecurityDataSaver dataSaver;

    public PresenterMain(ViewContainer view) {
        this.view = view;
        model = new ModelMain();
        dataSaver = SharedPreferenceWorker.getInstance();
    }

    public void checkState() {
        if (!model.isUserWasAuthorised()) {
            view.toFirstStartAppFragment();
        } else if (!model.isLoginstatus() && model.isUserWasAuthorised()) {
            if (model.isPinCodeExist()) {
                view.toPinCodeFragment();
            } else {
                view.toAuthoriseFragment();
            }
        }
    }

}
