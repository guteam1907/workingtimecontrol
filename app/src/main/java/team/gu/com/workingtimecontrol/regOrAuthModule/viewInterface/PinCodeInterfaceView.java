package team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface;

public interface PinCodeInterfaceView {
    void setPinCodeHint(String hint);

    void clearPinCodeData();

    ;

    void startMainActivityAndClearOldData();

    void setErrorRepairPinCodeLogin(String error);

    void setRepairAnswer(String serverAnswer);

    void setErrorRepairPinCodeNewPass(String error);

    void setAuthError(String error);
}
