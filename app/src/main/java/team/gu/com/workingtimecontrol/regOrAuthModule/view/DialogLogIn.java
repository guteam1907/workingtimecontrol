package team.gu.com.workingtimecontrol.regOrAuthModule.view;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import team.gu.com.workingtimecontrol.R;
import team.gu.com.workingtimecontrol.regOrAuthModule.AbleToChangeFragment;
import team.gu.com.workingtimecontrol.regOrAuthModule.presenter.DialogLoginPresenter;
import team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface.DialogLoginView;

/**
 * Диалог всплывающий для входа в приложение
 * ссылки на TextInputLayout вынесены для сообщения в них .setError()
 * для этого предлагаю создать класс InputChecker, где будем проверять валидность ввода(REGEX)
 */
public class DialogLogIn extends DialogFragment implements DialogLoginView {
    @BindView(R.id.log_in_data_edit_text)
    EditText logInData;
    @BindView(R.id.log_in_password_edit_text)
    EditText logInPass;
    @BindView(R.id.log_in_btn)
    Button loginBtn;
    @BindView(R.id.cancel_dialog_auth_btn)
    Button cancelDialog;
    @BindView(R.id.forgot_pass)

    TextView forgotPass;
    @BindView(R.id.login_password_layout)

    LinearLayout showloginDataLayout;
    @BindView(R.id.forgot_password_layout)
    LinearLayout forgotPassLayout;
    @BindView(R.id.log_in_data_il)
    TextInputLayout logInDataUserLayout;
    @BindView(R.id.log_in_password_il)
    TextInputLayout logInPassLayout;
    @BindView(R.id.repair_password_login)
    EditText repairPassLogin;
    @BindView(R.id.repair_password_login_il)
    TextInputLayout repairPasswordLoginLayout;
    @BindView(R.id.repair_new_password_et)
    EditText repairPassNewPassword;
    @BindView(R.id.repair_new_password_il)
    TextInputLayout repairNewPasswordLayout;
    private DialogLoginPresenter presenter;
    private AbleToChangeFragment ableToChangeFragment;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new DialogLoginPresenter(this);
        ableToChangeFragment = (AbleToChangeFragment) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_authentification, null);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.forgot_pass)
    public void forgotPasswordClick() {
        presenter.forgotPassClick();
    }

    @OnClick(R.id.cancel_dialog_auth_btn)
    public void cancelDialog() {
        getDialog().cancel();
    }

    @OnClick(R.id.log_in_btn)
    public void logInApp() {
        //TODO в презентере нужно выполнить проверку на валидность данных
        String userLogin = logInData.getText().toString();
        String userPassword = logInData.getText().toString();
        presenter.loginBtnClick(userLogin, userPassword);
    }

    @Override
    public void forgotPassBtnClick(boolean userForgotPassword) {
        if (userForgotPassword) {
            showloginDataLayout.setVisibility(View.GONE);
            forgotPassLayout.setVisibility(View.VISIBLE);
            forgotPass.setTextColor(Color.BLUE);
        } else {
            showloginDataLayout.setVisibility(View.VISIBLE);
            forgotPassLayout.setVisibility(View.GONE);
            forgotPass.setTextColor(Color.BLACK);
        }
    }

    @Override
    public void getRepeatPassData() {
        String login = repairPassLogin.getText().toString();
        String newPassword = repairPassNewPassword.getText().toString();
        presenter.resetPasswordClick(login, newPassword);
    }

    @Override
    public void toPinCodeFragment(String password) {
        ableToChangeFragment.changeFragments(R.id.main_container, FragmentPinCode.newInstance(password), FragmentPinCode.TAG, false);
    }
}
