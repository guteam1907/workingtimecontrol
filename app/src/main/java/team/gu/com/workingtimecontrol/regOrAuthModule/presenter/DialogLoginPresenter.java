package team.gu.com.workingtimecontrol.regOrAuthModule.presenter;

import team.gu.com.workingtimecontrol.entity.LeaderUser;
import team.gu.com.workingtimecontrol.regOrAuthModule.model.RegOrAuthModel;
import team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface.DialogLoginView;
import team.gu.com.workingtimecontrol.security.PaperBooksReaderWriter;

public class DialogLoginPresenter {
    private DialogLoginView view;
    private RegOrAuthModel model;

    public DialogLoginPresenter(DialogLoginView view) {
        this.view = view;
        model = RegOrAuthModel.getInstance();
    }

    public void forgotPassClick() {
        model.setUserForgotPassword(!model.isUserForgotPassword());
        view.forgotPassBtnClick(model.isUserForgotPassword());
    }

    public void loginBtnClick(String userLogin, String userPassword) {
        if (!model.isUserForgotPassword()) {
            if (model.isUserLeader()) {
                //TODO авторизовываемся как лидер
                LeaderUser leaderUser = PaperBooksReaderWriter.readDataFromBook(PaperBooksReaderWriter.PAPER_USERS, LeaderUser.key);
                if (leaderUser.getLogin().equals(userLogin) && leaderUser.getPassword() == userPassword) {
                    view.toPinCodeFragment(userPassword);
                }
            } else if (model.isUserWorker()) {
                //TODO Авторизовываемся как работник
            }
            //TODO переходим к вводу пинкода если пользователь правильно ввел данные
        } else {
            view.getRepeatPassData();
        }
    }

    public void resetPasswordClick(String login, String newPassword) {
        if (model.isUserLeader()) {
            //TODO сбрасываем пароль лидера
        } else if (model.isUserWorker()) {
            //TODO сбрасываем пароль работника
        }
    }
}
