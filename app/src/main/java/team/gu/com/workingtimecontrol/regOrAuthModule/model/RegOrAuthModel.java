package team.gu.com.workingtimecontrol.regOrAuthModule.model;

public class RegOrAuthModel {
    private static RegOrAuthModel ourInstance;
    private boolean isUserWantRegister;
    private boolean isUserWantLogIn;
    private boolean isUserLeader;
    private boolean isUserWorker;
    private boolean isUserForgotPassword;

    private RegOrAuthModel() {
    }

    public static RegOrAuthModel getInstance() {
        if (ourInstance == null)
            ourInstance = new RegOrAuthModel();
        return ourInstance;
    }

    public boolean isUserForgotPassword() {
        return isUserForgotPassword;
    }

    public void setUserForgotPassword(boolean userForgotPassword) {
        this.isUserForgotPassword = userForgotPassword;
    }

    public boolean isUserWantRegister() {
        return isUserWantRegister;
    }

    public void setUserWantRegister(boolean userWantRegister) {
        isUserWantRegister = userWantRegister;
    }

    public boolean isUserWantLogIn() {
        return isUserWantLogIn;
    }

    public void setUserWantLogIn(boolean userWantLogIn) {
        isUserWantLogIn = userWantLogIn;
    }

    public boolean isUserLeader() {
        return isUserLeader;
    }

    public void setUserLeader(boolean userLeader) {
        isUserLeader = userLeader;
    }

    public boolean isUserWorker() {
        return isUserWorker;
    }

    public void setUserWorker(boolean userWorker) {
        isUserWorker = userWorker;
    }
}
