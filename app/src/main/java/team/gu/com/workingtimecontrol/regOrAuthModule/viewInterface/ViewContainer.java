package team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface;

public interface ViewContainer {
    void toFirstStartAppFragment();

    void toPinCodeFragment();

    void toAuthoriseFragment();
}
