package team.gu.com.workingtimecontrol.regOrAuthModule.model;

public class ModelPinCode {
    private String firstPinCode;
    private StringBuilder currentPincode;
    private String login;
    private String password;
    private boolean userIsForgotPassword;

    public ModelPinCode() {
        currentPincode = new StringBuilder();
    }

    public String getFirstPinCode() {
        return firstPinCode;
    }

    public void setFirstPinCode(String firstPinCode) {
        this.firstPinCode = firstPinCode;
    }

    public String getCurrentPincode() {
        return currentPincode.toString();
    }

    public void addNumToPinCode(String num) {
        currentPincode.append(num);
    }

    public void setPincode(String pincode) {
        currentPincode = new StringBuilder(pincode);
    }

    public void deleteLastCharInPinCode() {
        currentPincode.deleteCharAt(currentPincode.lastIndexOf(currentPincode.toString()));
    }

    public void deletePinCode() {
        currentPincode.delete(0, 5);
    }

    public void deleteFirstPinCode() {
        firstPinCode = null;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setUserIsForgotPassword(boolean userIsForgotPassword) {
        this.userIsForgotPassword = userIsForgotPassword;
    }
}
