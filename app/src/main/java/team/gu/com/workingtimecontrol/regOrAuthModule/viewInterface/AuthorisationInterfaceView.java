package team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface;

public interface AuthorisationInterfaceView {
    void forgotPassBtnClick(boolean isForgotPass);

    void setLoginError(String result);

    void setPasswordError(String result);


    void startMainActivityAndClearOldData();

    void setRepairPassLoginInputError(String error);

    void setRepairPassNewPasswordInputError(String error);

    void setAuthError(String error);
}
