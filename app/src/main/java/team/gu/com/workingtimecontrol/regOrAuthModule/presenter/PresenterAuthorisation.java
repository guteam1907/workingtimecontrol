package team.gu.com.workingtimecontrol.regOrAuthModule.presenter;

import team.gu.com.workingtimecontrol.entity.LeaderUser;
import team.gu.com.workingtimecontrol.entity.WorkerUser;
import team.gu.com.workingtimecontrol.regOrAuthModule.model.ModelAuthorisation;
import team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface.AuthorisationInterfaceView;
import team.gu.com.workingtimecontrol.security.PaperBooksReaderWriter;
import team.gu.com.workingtimecontrol.security.SecurityDataSaver;
import team.gu.com.workingtimecontrol.security.SharedPreferenceWorker;

public class PresenterAuthorisation {
    private AuthorisationInterfaceView view;
    private ModelAuthorisation model;
    private SecurityDataSaver dataSaver;

    public PresenterAuthorisation(AuthorisationInterfaceView view) {
        this.view = view;
        dataSaver = SharedPreferenceWorker.getInstance();
        model = new ModelAuthorisation();
    }

    public void forgotPass() {
        model.setForgotBassword(!model.isForgotBassword());
        view.forgotPassBtnClick(model.isForgotBassword());
    }

    public boolean isUserForgotPassword() {
        return model.isForgotBassword();
    }

    public void authorisation(String login, String password) {
        model.setLoginData(login);
        model.setPassword(password);
        //TODO выполнить авторизацию
        if (dataSaver.checkWorkerState()) {
            WorkerUser user = PaperBooksReaderWriter.readDataFromBook(PaperBooksReaderWriter.PAPER_USERS, WorkerUser.key);
            String userServerLogin = user.getLogin();
            String userServerPassword = user.getPassword();
            if (userServerLogin.equals(login) && userServerPassword.equals(password))
                view.startMainActivityAndClearOldData();
            else view.setAuthError("Не верный пароль");
        }
        if (dataSaver.checkLeaderState()) {
            LeaderUser user = PaperBooksReaderWriter.readDataFromBook(PaperBooksReaderWriter.PAPER_USERS, LeaderUser.key);
            String userServerLogin = user.getLogin();
            String userServerPassword = user.getPassword();
            if (userServerLogin.equals(login) && userServerPassword.equals(password))
                view.startMainActivityAndClearOldData();
            else view.setAuthError("Не верный пароль");
        }

    }

    public void resetPassword(String repairData, String newPassword) {
        model.setLoginData(repairData);
        model.setPassword(newPassword);
    }


}
