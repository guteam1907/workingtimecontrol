package team.gu.com.workingtimecontrol.regOrAuthModule.model;


public class ModelAuthorisation {
    private boolean isForgotBassword;
    private String loginData;
    private String password;
    private String smsCode;

    public ModelAuthorisation() {

    }

    public boolean isForgotBassword() {
        return isForgotBassword;
    }

    public void setForgotBassword(boolean forgotBassword) {
        isForgotBassword = forgotBassword;
    }

    public String getLoginData() {
        return loginData;
    }

    public void setLoginData(String loginData) {
        this.loginData = loginData;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(String smsCode) {
        this.smsCode = smsCode;
    }

}
