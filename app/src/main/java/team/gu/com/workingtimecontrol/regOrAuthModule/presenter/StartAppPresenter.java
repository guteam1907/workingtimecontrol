package team.gu.com.workingtimecontrol.regOrAuthModule.presenter;

import team.gu.com.workingtimecontrol.regOrAuthModule.model.RegOrAuthModel;
import team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface.StartAppInt;

public class StartAppPresenter {
    private StartAppInt view;
    private RegOrAuthModel model;

    public StartAppPresenter(StartAppInt view) {
        this.view = view;
        model = RegOrAuthModel.getInstance();
    }

    public void signInChoose() {
        model.setUserWantLogIn(true);
        view.setLogBtnInvisible(true);
        view.setTypeBtnInvisible(false);
    }

    public void signUpChoose() {
        model.setUserWantRegister(true);
        view.setLogBtnInvisible(true);
        view.setTypeBtnInvisible(false);
    }

    public void asWorkerChoose() {
        model.setUserWorker(true);
        if (model.isUserWantRegister()) {
            view.toRegistration();
        } else {
            view.showLoginDialog();
        }
    }

    public void asLeaderChoose() {
        model.setUserLeader(true);
        if (model.isUserWantRegister()) {
            view.toRegistration();
        } else {
            view.showLoginDialog();
        }
    }

}
