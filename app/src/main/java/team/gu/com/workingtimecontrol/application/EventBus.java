package team.gu.com.workingtimecontrol.application;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Event bus - для сообзения по приложению
 * вызывать через App.class
 */
public class EventBus {
    private PublishSubject<Object> bus = PublishSubject.create();

    public EventBus() {
    }

    public void send(Object o) {
        bus.onNext(o);
    }

    public Observable<Object> toObservable() {
        return bus;
    }
}