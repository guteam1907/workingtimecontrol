package team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface;

public interface DialogLoginView {
    void forgotPassBtnClick(boolean userForgotPassword);

    void getRepeatPassData();

    void toPinCodeFragment(String password);
}
