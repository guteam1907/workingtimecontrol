package team.gu.com.workingtimecontrol.security;

/**
 * Шифрование
 */
public class XORCryptographer {

    public static byte[] encode(String value, String pKey) {
        byte[] txt = value.getBytes();
        byte[] key = pKey.getBytes();
        byte[] res = new byte[value.length()];
        for (int i = 0; i < txt.length; i++) {
            res[i] = (byte) (txt[i] ^ key[i % key.length]);
        }
        return res;
    }

    public static String decode(byte[] encodedValue, String pKey) {
        byte[] res = new byte[encodedValue.length];
        byte[] key = pKey.getBytes();
        for (int i = 0; i < encodedValue.length; i++) {
            res[i] = (byte) (encodedValue[i] ^ key[i % key.length]);
        }
        return new String(res);
    }
}
