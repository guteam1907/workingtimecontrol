package team.gu.com.workingtimecontrol.regOrAuthModule.model;


import team.gu.com.workingtimecontrol.security.SecurityDataSaver;
import team.gu.com.workingtimecontrol.security.SharedPreferenceWorker;

public class ModelMain {
    private SecurityDataSaver dataSaver;
    private boolean userWasAuthorised;
    private boolean isPinCodeExist;
    private boolean loginstatus;

    public ModelMain() {
        dataSaver = SharedPreferenceWorker.getInstance();
        userWasAuthorised = dataSaver.checkAuthorised();
        isPinCodeExist = dataSaver.checkPinCodeExisting();
    }

    public boolean isUserWasAuthorised() {
        return userWasAuthorised;
    }

    public void setUserWasAuthorised(boolean userWasAuthorised) {
        this.userWasAuthorised = userWasAuthorised;
    }

    public boolean isPinCodeExist() {
        return isPinCodeExist;
    }

    public void setPinCodeExist(boolean pinCodeExist) {
        isPinCodeExist = pinCodeExist;
    }

    public boolean isLoginstatus() {
        return loginstatus;
    }

    public void setLoginstatus(boolean loginstatus) {
        this.loginstatus = loginstatus;
    }
}
