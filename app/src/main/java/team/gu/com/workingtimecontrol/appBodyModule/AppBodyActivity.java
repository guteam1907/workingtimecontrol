package team.gu.com.workingtimecontrol.appBodyModule;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import team.gu.com.workingtimecontrol.R;
import team.gu.com.workingtimecontrol.security.SharedPreferenceWorker;

public class AppBodyActivity extends AppCompatActivity {
    @BindView(R.id.type)
    TextView type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_body);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        if (SharedPreferenceWorker.getInstance().checkLeaderState()) {
            type.setText("Вы вошли как лидер");
        } else {
            type.setText("Вы вошли как работник");
        }
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());
    }

}
