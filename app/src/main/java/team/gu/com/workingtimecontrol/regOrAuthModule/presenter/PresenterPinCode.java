package team.gu.com.workingtimecontrol.regOrAuthModule.presenter;


import team.gu.com.workingtimecontrol.entity.LeaderUser;
import team.gu.com.workingtimecontrol.entity.WorkerUser;
import team.gu.com.workingtimecontrol.regOrAuthModule.model.ModelPinCode;
import team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface.PinCodeInterfaceView;
import team.gu.com.workingtimecontrol.security.PaperBooksReaderWriter;
import team.gu.com.workingtimecontrol.security.SecurityDataSaver;
import team.gu.com.workingtimecontrol.security.SharedPreferenceWorker;
import team.gu.com.workingtimecontrol.security.XORCryptographer;

public class PresenterPinCode {
    private PinCodeInterfaceView view;
    private ModelPinCode model;
    private SecurityDataSaver dataSaver;


    public PresenterPinCode(PinCodeInterfaceView view) {
        this.view = view;
        dataSaver = SharedPreferenceWorker.getInstance();
        model = new ModelPinCode();
    }

    public void setFirstPincode(String firstPinCode) {
        model.setFirstPinCode(firstPinCode);
    }

    public String getFirstPinCode() {
        return model.getFirstPinCode();
    }

    public void addNumToPinCode(String num) {
        model.addNumToPinCode(num);
    }

    public String getCurrentPinCode() {
        return model.getCurrentPincode();
    }

    public void setCurrentPinCode(String currentPinCode) {
        model.setPincode(currentPinCode);
    }

    public void deleteLastCharInPinCode() {
        model.deleteLastCharInPinCode();
    }


    /**
     * Если нет то проверяем первый раз ли пользователь ввел пинкод
     */
    public boolean isUserInputPinCodeFirstTime() {
        if (model.getFirstPinCode() == null) {
            model.setFirstPinCode(model.getCurrentPincode());
            model.deletePinCode();
            return true;
        } else return false;
    }

    /**
     * если пользовательсввел пинкод 2жды то сверяем оба пинккода, если они совпадают то запоминаем его
     * и входим в приложение
     */
    public boolean isUserInputTheSamePinCodeSecondTime() {
        if (model.getFirstPinCode().equals(model.getCurrentPincode())) {
            return true;
        } else return false;
    }

    public void setPassword(String password) {
        model.setPassword(password);
    }

    public void pinCodeInputComplete() {
        if (isUserInputPinCodeFirstTime()) {
            view.setPinCodeHint("Введите ваш пин-код еще раз");
            view.clearPinCodeData();
            return;
        } else {
            if (isUserInputTheSamePinCodeSecondTime()) {
                dataSaver.savePinCodeExistState(true);
                /**Шифруем пароль пинкодом и запоминаем шифр*/
                byte[] ciper = XORCryptographer.encode(model.getPassword(), model.getCurrentPincode());
                dataSaver.saveChiper(new String(ciper));
                view.startMainActivityAndClearOldData();
                return;
            } else {
                model.deleteFirstPinCode();
                model.deletePinCode();
                view.setPinCodeHint("Пин-код не совпадает с предыдущим \n повторите ввод");
                view.clearPinCodeData();
                return;
            }
        }
    }

    /**
     * запрос на сервер для сброса пароля и пинкода
     */
    public void repairPinCode(String data, String newPassword) {
        model.setLogin(data);
        model.setPassword(newPassword);
    }

    /**
     * расшифровываем пароль и проводим авторизацию, если успешна то входим в приложение
     */
    public void authoriseByPinCode(String chiper, String login) {
        String password = XORCryptographer.decode(chiper.getBytes(), model.getCurrentPincode());
        if (dataSaver.checkWorkerState()) {
            WorkerUser user = PaperBooksReaderWriter.readDataFromBook(PaperBooksReaderWriter.PAPER_USERS, WorkerUser.key);
            String userServerLogin = user.getLogin();
            String userServerPassword = user.getPassword();
            if (userServerLogin.equals(login) && userServerPassword.equals(password))
                view.startMainActivityAndClearOldData();
            else view.setAuthError("Не верный пин-код");
        }
        if (dataSaver.checkLeaderState()) {
            LeaderUser user = PaperBooksReaderWriter.readDataFromBook(PaperBooksReaderWriter.PAPER_USERS, LeaderUser.key);
            String userServerLogin = user.getLogin();
            String userServerPassword = user.getPassword();
            if (userServerLogin.equals(login) && userServerPassword.equals(password))
                view.startMainActivityAndClearOldData();
            else view.setAuthError("Не верный пин-код");
        }
    }

}
