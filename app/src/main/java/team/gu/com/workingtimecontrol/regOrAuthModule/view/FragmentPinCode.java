package team.gu.com.workingtimecontrol.regOrAuthModule.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import team.gu.com.workingtimecontrol.R;
import team.gu.com.workingtimecontrol.appBodyModule.AppBodyActivity;
import team.gu.com.workingtimecontrol.regOrAuthModule.AbleToChangeFragment;
import team.gu.com.workingtimecontrol.regOrAuthModule.OnBackPressedListener;
import team.gu.com.workingtimecontrol.regOrAuthModule.presenter.PresenterPinCode;
import team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface.PinCodeInterfaceView;
import team.gu.com.workingtimecontrol.security.SharedPreferenceWorker;


/**
 * Общий фрагмент для всего приложения, вызывается сразу после регистрации для создания пинкода
 * и каждый раз при входв в приложение(если пинкод установлен)
 * Не успел переделать под butterknife сорри
 */
public class FragmentPinCode extends Fragment implements View.OnClickListener, OnBackPressedListener, PinCodeInterfaceView {
    public static final String TAG = "Пин-код";
    private static final String PASSWORD_KEY = "PASSWORD_KEY";
    private static final String PINCODE_VALUE = "PINCODE_VALUE";
    private static final String FIRST_PINCODE_VALUE = "FIRST_PINCODE_VALUE";
    private static final String PINCODE_HINT = "PINCODE_HINT";
    private final int EMPTY = R.drawable.presence_invisible;
    private final int FULL = R.drawable.presence_online;
    private TextView pinCodeFragmentHint;
    private TextView forgotPin;
    private List<ImageButton> views;
    private AbleToChangeFragment callBackFragment;
    private Button skipPinCodeInput;
    private Context context;
    private PresenterPinCode presenter;
    private TextInputLayout userDataLayout;
    private TextInputLayout newPasswordLayout;
    private EditText newPassword;
    private EditText usersData;
    private TextView hint;
    private AlertDialog alertDialog;

    /**
     * Получение экземпляра фрагмента
     */
    public static FragmentPinCode newInstance(String password) {
        FragmentPinCode fragment = new FragmentPinCode();
        Bundle args = new Bundle();
        args.putString(PASSWORD_KEY, password);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBackFragment = (AbleToChangeFragment) context;
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pin_code, container, false);
        presenter = new PresenterPinCode(this);
        presenter.setPassword(getArguments().getString(PASSWORD_KEY));
        initViews(view);
        initListeners();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (savedInstanceState != null) {
            rewriteState(savedInstanceState);
        }
        return view;
    }

    private void initListeners() {
        forgotPin.setOnClickListener(v -> {
            if (SharedPreferenceWorker.getInstance().checkAuthorised())
                showDialog();
        });
        skipPinCodeInput.setOnClickListener(v -> {
            SharedPreferenceWorker.getInstance().savePinCodeExistState(false);
            startMainActivityAndClearOldData();
        });
    }

    /**
     * загрузить ранее сохраненной есостояние при пересоздании фрагмента
     */
    private void rewriteState(Bundle savedInstanceState) {
        String currentPinCode = savedInstanceState.getString(PINCODE_VALUE);
        presenter.setCurrentPinCode(currentPinCode);
        for (int i = 0; i < currentPinCode.length(); i++) {
            views.get(i).setImageResource(FULL);
        }
        if (!SharedPreferenceWorker.getInstance().checkPinCodeExisting()) {
            presenter.setFirstPincode(savedInstanceState.getString(FIRST_PINCODE_VALUE));
            pinCodeFragmentHint.setText(savedInstanceState.getString(PINCODE_HINT));
        }
    }

    /**
     * сохраняем текущее состояние фрагмента
     */
    @SuppressLint("ResourceType")
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(PINCODE_VALUE, presenter.getCurrentPinCode());
        if (!SharedPreferenceWorker.getInstance().checkPinCodeExisting()) {
            String firstPinCode = presenter.getFirstPinCode();
            if (firstPinCode != null) {
                outState.putString(FIRST_PINCODE_VALUE, firstPinCode);
            }
            outState.putString(PINCODE_HINT, pinCodeFragmentHint.getText().toString());
        }
        super.onSaveInstanceState(outState);
    }

    /**
     * Инициализируем вьюшки
     */
    private void initViews(View view) {
        pinCodeFragmentHint = view.findViewById(R.id.pin_code_fragment_hint);
        forgotPin = view.findViewById(R.id.forgot_pin);
        views = new ArrayList<>();
        views.add(view.findViewById(R.id.pin_enter_0));
        views.add(view.findViewById(R.id.pin_enter_1));
        views.add(view.findViewById(R.id.pin_enter_2));
        views.add(view.findViewById(R.id.pin_enter_3));
        views.add(view.findViewById(R.id.pin_enter_4));
        Button zero = view.findViewById(R.id.pin_num_0);
        zero.setOnClickListener(this);
        Button one = view.findViewById(R.id.pin_num_1);
        one.setOnClickListener(this);
        Button two = view.findViewById(R.id.pin_num_2);
        two.setOnClickListener(this);
        Button three = view.findViewById(R.id.pin_num_3);
        three.setOnClickListener(this);
        Button four = view.findViewById(R.id.pin_num_4);
        four.setOnClickListener(this);
        Button five = view.findViewById(R.id.pin_num_5);
        five.setOnClickListener(this);
        Button six = view.findViewById(R.id.pin_num_6);
        six.setOnClickListener(this);
        Button seven = view.findViewById(R.id.pin_num_7);
        seven.setOnClickListener(this);
        Button eight = view.findViewById(R.id.pin_num_8);
        eight.setOnClickListener(this);
        Button nine = view.findViewById(R.id.pin_num_9);
        nine.setOnClickListener(this);
        ImageButton delete = view.findViewById(R.id.pin_delete);
        delete.setOnClickListener(this);
        skipPinCodeInput = view.findViewById(R.id.skip_pincode_input_btn);
        if (!SharedPreferenceWorker.getInstance().checkPinCodeExisting()) {
            pinCodeFragmentHint.setText("Придумайте ваш пин-код");
            forgotPin.setVisibility(View.GONE);
        } else {
            skipPinCodeInput.setVisibility(View.GONE);
        }
    }

    /**
     * Обрабатываем клик по вьюшкам
     */
    @Override
    public void onClick(View v) {
        String pinCode = presenter.getCurrentPinCode();
        int id = v.getId();
        if (id == R.id.pin_num_0 || id == R.id.pin_num_1 || id == R.id.pin_num_2 || id == R.id.pin_num_3
                || id == R.id.pin_num_4 || id == R.id.pin_num_5 || id == R.id.pin_num_6 || id == R.id.pin_num_7
                || id == R.id.pin_num_8 || id == R.id.pin_num_9) {
            pinCode = presenter.getCurrentPinCode();
            if (pinCode.length() < 5) {
                views.get(pinCode.length()).setImageResource(FULL);
                switch (id) {
                    case R.id.pin_num_0:
                        presenter.addNumToPinCode(String.valueOf(0));
                        break;
                    case R.id.pin_num_1:
                        presenter.addNumToPinCode(String.valueOf(1));
                        break;
                    case R.id.pin_num_2:
                        presenter.addNumToPinCode(String.valueOf(2));
                        break;
                    case R.id.pin_num_3:
                        presenter.addNumToPinCode(String.valueOf(3));
                        break;
                    case R.id.pin_num_4:
                        presenter.addNumToPinCode(String.valueOf(4));
                        break;
                    case R.id.pin_num_5:
                        presenter.addNumToPinCode(String.valueOf(5));
                        break;
                    case R.id.pin_num_6:
                        presenter.addNumToPinCode(String.valueOf(6));
                        break;
                    case R.id.pin_num_7:
                        presenter.addNumToPinCode(String.valueOf(7));
                        break;
                    case R.id.pin_num_8:
                        presenter.addNumToPinCode(String.valueOf(8));
                        break;
                    case R.id.pin_num_9:
                        presenter.addNumToPinCode(String.valueOf(9));
                        break;
                }
            }
        }
        if (id == R.id.pin_delete) {
            pinCode = presenter.getCurrentPinCode();
            if (pinCode.length() != 0) {
                views.get(pinCode.length() - 1).setImageResource(EMPTY);
                presenter.deleteLastCharInPinCode();
            }
        }
        if (pinCode.length() == 4) {
            /**Проверка был ли авторизоавн пользователь*/
            if (!SharedPreferenceWorker.getInstance().checkPinCodeExisting()) {
                /**Если нет то проверяем первый раз ли он ввел пинкод*/
                presenter.pinCodeInputComplete();
            } else {
                /**Если пользователь был авторизован то расшифровываем его пинкодом пароль и шлем авторизацию*/
                //TODO закоментирован код на случай хранения пинкода на телефоне, и его проверка перед отправкой
                //TODO расшифрованного пароля на сервер
                String chiper = SharedPreferenceWorker.getInstance().getUserChiper();
                String login = SharedPreferenceWorker.getInstance().getUserLogin();
                presenter.authoriseByPinCode(chiper, login);
            }
        }
        Log.d(TAG, "pinCode:" + pinCode);
    }

    /**
     * Если пользователь уже был авторизован значит - этот фрагмент стартует после запуска приложения
     * и при нажатии кнопки назад - выход из приложения
     */
    @Override
    public void onBackPressed() {
        callBackFragment.removeFragment(this);
        if (SharedPreferenceWorker.getInstance().checkAuthorised()) {
            getActivity().finish();
        } else getFragmentManager().popBackStack();
    }

    /**
     * Переход в тело приложения
     */
    public void startMainActivityAndClearOldData() {
        Intent intent = new Intent(getActivity(), AppBodyActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void setErrorRepairPinCodeLogin(String error) {
        userDataLayout.setError(error);
    }

    @Override
    public void setRepairAnswer(String serverAnswer) {
        hint.setText(serverAnswer);
    }

    @Override
    public void setErrorRepairPinCodeNewPass(String error) {
        newPasswordLayout.setError(error);
    }

    @Override
    public void setAuthError(String error) {
        pinCodeFragmentHint.setText(error);
        pinCodeFragmentHint.setTextColor(Color.RED);
    }

    @Override
    public void setPinCodeHint(String hint) {
        pinCodeFragmentHint.setText(hint);
    }

    @Override
    public void clearPinCodeData() {
        for (int i = 0; i < views.size(); i++) {
            views.get(i).setImageResource(EMPTY);
        }
    }

    /**
     * Показать диалоговое окно если пользователь нажал ЗАБЫЛИ ПИН-КОД
     */
    private void showDialog() {
        View dialog = getLayoutInflater().inflate(R.layout.dialog_forgot_pin_code, null);
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(getContext());
        mDialogBuilder.setView(dialog);
        usersData = dialog.findViewById(R.id.reset_pin_code_login_et);
        newPassword = dialog.findViewById(R.id.reset_pin_new_password_et);
        Button resetPinBtn = dialog.findViewById(R.id.send_reset_pin_btn);
        Button cancelDialog = dialog.findViewById(R.id.cancel_dialog_pin_btn);
        hint = dialog.findViewById(R.id.forgot_pin_hint_tv);
        userDataLayout = dialog.findViewById(R.id.reset_pin_login_il);
        newPasswordLayout = dialog.findViewById(R.id.reset_pin_new_password_il);
        mDialogBuilder
                .setCancelable(true);
        alertDialog = mDialogBuilder.create();
        alertDialog.show();
        resetPinBtn.setOnClickListener(v -> {
            presenter.repairPinCode(usersData.getText().toString(), newPassword.getText().toString());
        });
        cancelDialog.setOnClickListener(v -> alertDialog.cancel());
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
