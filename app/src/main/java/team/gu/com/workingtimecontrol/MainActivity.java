package team.gu.com.workingtimecontrol;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import team.gu.com.workingtimecontrol.regOrAuthModule.AbleToChangeFragment;
import team.gu.com.workingtimecontrol.regOrAuthModule.OnBackPressedListener;
import team.gu.com.workingtimecontrol.regOrAuthModule.presenter.PresenterMain;
import team.gu.com.workingtimecontrol.regOrAuthModule.view.FragmentAuthorisation;
import team.gu.com.workingtimecontrol.regOrAuthModule.view.FragmentPinCode;
import team.gu.com.workingtimecontrol.regOrAuthModule.view.FragmentStartApp;
import team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface.ViewContainer;

/**
 * При запуске активности вызываем presenter.CheckState()
 * если пользователь был авторизован и у него был введен пинкод то переходим на фрагмент пинкода, если аторизован
 * и не введен пинкод то переходим на фрагмент авторизации по логин\паролю, если не то и не другое то запускаем
 * FragmentStartApp
 */
public class MainActivity extends AppCompatActivity implements AbleToChangeFragment, ViewContainer {
    @BindView(R.id.appbar)
    AppBarLayout appBar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    FragmentManager fm;
    PresenterMain presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        fm = getSupportFragmentManager();
        presenter = new PresenterMain(this);
        presenter.checkState();
    }

    /**
     * замена одного фрагмента другим
     */
    @Override
    public void changeFragments(int containerViewId, Fragment newFragment, String tag, boolean addToBackStack) {
        if (addToBackStack) {
            fm.beginTransaction().replace(containerViewId, newFragment, tag).addToBackStack(tag).commit();
        } else {
            fm.beginTransaction().replace(containerViewId, newFragment).commit();
        }
        toolbar.setTitle(tag);
    }

    /**
     * удаление фрагмента с backStack
     */
    @Override
    public void removeFragment(Fragment removingFragment) {
        fm.beginTransaction().remove(removingFragment).commit();
    }

    /**
     * слушатель кнопки назад для активити, если нужно вызвать особое действие из фрагмента при клике back
     */
    @Override
    public void onBackPressed() {
        OnBackPressedListener backPressedListener = null;
        for (Fragment fragment : fm.getFragments()) {
            if (fragment instanceof OnBackPressedListener) {
                backPressedListener = (OnBackPressedListener) fragment;
                break;
            }
        }
        if (backPressedListener != null) {
            backPressedListener.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void toFirstStartAppFragment() {
        changeFragments(R.id.main_container, FragmentStartApp.newInstance(), FragmentStartApp.TAG, false);
    }

    @Override
    public void toPinCodeFragment() {
        changeFragments(R.id.main_container, FragmentPinCode.newInstance(null), FragmentPinCode.TAG, false);
    }

    @Override
    public void toAuthoriseFragment() {
        changeFragments(R.id.main_container, FragmentAuthorisation.newInstance(), FragmentAuthorisation.TAG, false);
    }
}
