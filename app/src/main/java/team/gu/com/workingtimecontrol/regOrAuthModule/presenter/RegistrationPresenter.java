package team.gu.com.workingtimecontrol.regOrAuthModule.presenter;

import team.gu.com.workingtimecontrol.entity.LeaderUser;
import team.gu.com.workingtimecontrol.entity.WorkerUser;
import team.gu.com.workingtimecontrol.regOrAuthModule.model.RegOrAuthModel;
import team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface.RegistrationView;
import team.gu.com.workingtimecontrol.security.PaperBooksReaderWriter;
import team.gu.com.workingtimecontrol.security.SecurityDataSaver;
import team.gu.com.workingtimecontrol.security.SharedPreferenceWorker;

public class RegistrationPresenter {
    private RegistrationView view;
    private RegOrAuthModel model;
    private SecurityDataSaver dataSaver;

    public RegistrationPresenter(RegistrationView view) {
        this.view = view;
        model = RegOrAuthModel.getInstance();
        dataSaver = SharedPreferenceWorker.getInstance();
    }

    public void registration(String login, String password) {
        //TODO проверка данных на валидность
        if (model.isUserWorker()) {
            WorkerUser workerUser = new WorkerUser();
            workerUser.setLogin(login);
            workerUser.setPassword(password);
            PaperBooksReaderWriter.writeDataToBook(workerUser, WorkerUser.key, PaperBooksReaderWriter.PAPER_USERS);
            dataSaver.saveAuthState(true);
            dataSaver.saveWorkerState(true);
            dataSaver.saveUserLogin(login);
            dataSaver.saveUserPassword(password);
        } else if (model.isUserLeader()) {
            LeaderUser leaderUser = new LeaderUser();
            leaderUser.setLogin(login);
            leaderUser.setPassword(password);
            PaperBooksReaderWriter.writeDataToBook(leaderUser, LeaderUser.key, PaperBooksReaderWriter.PAPER_USERS);
            dataSaver.saveAuthState(true);
            dataSaver.saveLeaderState(true);
            dataSaver.saveUserLogin(login);
            dataSaver.saveUserPassword(password);
        }
        view.toPinCodeFragment(password);
    }
}
