package team.gu.com.workingtimecontrol.regOrAuthModule.view;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import team.gu.com.workingtimecontrol.R;
import team.gu.com.workingtimecontrol.regOrAuthModule.AbleToChangeFragment;
import team.gu.com.workingtimecontrol.regOrAuthModule.presenter.RegistrationPresenter;
import team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface.RegistrationView;

public class FragmentRegistration extends Fragment implements RegistrationView {
    public static final String TAG = "Регистрация";
    @BindView(R.id.feauture_login_et)
    EditText login;
    @BindView(R.id.feauture_login_til)
    TextInputLayout LoginLayout;
    @BindView(R.id.feauture_password_et)
    EditText password;
    @BindView(R.id.feauture_password_til)
    TextInputLayout passwordLatout;
    @BindView(R.id.registred_btn)
    Button registration;
    private RegistrationPresenter presenter;
    private AbleToChangeFragment ableToChangeFragment;

    public static Fragment newInstance() {
        FragmentRegistration fragmentRegistration = new FragmentRegistration();
        Bundle bundle = new Bundle();
        fragmentRegistration.setArguments(bundle);
        return fragmentRegistration;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new RegistrationPresenter(this);
        ableToChangeFragment = (AbleToChangeFragment) context;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.registred_btn)
    public void registrationClick() {
        String login = this.login.getText().toString();
        String password = this.password.getText().toString();
        presenter.registration(login, password);
    }


    @Override
    public void toPinCodeFragment(String password) {
        ableToChangeFragment.changeFragments(R.id.main_container, FragmentPinCode.newInstance(password), FragmentPinCode.TAG, false);
    }


}
