package team.gu.com.workingtimecontrol.regOrAuthModule;

/**
 * Интерфейс - слушатель кнопки НАЗАД для фрагментов
 */
public interface OnBackPressedListener {
    void onBackPressed();
}
