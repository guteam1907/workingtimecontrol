package team.gu.com.workingtimecontrol.application;


import android.app.Application;

import io.paperdb.Paper;

public class App extends Application {
    private static App instance = null;
    private EventBus eventBus;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Paper.init(this);
        eventBus = new EventBus();
    }

    public EventBus getEventBus() {
        return eventBus;
    }
}
