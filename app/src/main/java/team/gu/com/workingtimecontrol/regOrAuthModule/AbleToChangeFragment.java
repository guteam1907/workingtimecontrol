package team.gu.com.workingtimecontrol.regOrAuthModule;

import android.support.v4.app.Fragment;

/**
 * Интерфейс для смены или удаления фрагмента
 */
public interface AbleToChangeFragment {
    void changeFragments(int containerViewId, Fragment newFragment, String tag, boolean addToBackStack);

    void removeFragment(Fragment removeFragment);
}

