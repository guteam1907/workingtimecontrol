package team.gu.com.workingtimecontrol.regOrAuthModule.view;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import team.gu.com.workingtimecontrol.R;
import team.gu.com.workingtimecontrol.regOrAuthModule.AbleToChangeFragment;
import team.gu.com.workingtimecontrol.regOrAuthModule.OnBackPressedListener;
import team.gu.com.workingtimecontrol.regOrAuthModule.presenter.StartAppPresenter;
import team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface.StartAppInt;

public class FragmentStartApp extends Fragment implements StartAppInt, OnBackPressedListener {
    public static final String TAG = "Название приложения, пока нет";
    @BindView(R.id.sign_in_btn)
    Button signInBtn;
    @BindView(R.id.sign_up_btn)
    Button signUpBtn;
    @BindView(R.id.as_leader)
    Button asLeaderBtn;
    @BindView(R.id.as_worker)
    Button asWorkerBtn;
    @BindView(R.id.log_btns_layout)
    LinearLayout logBtnsLayout;
    @BindView(R.id.type_btns_layout)
    LinearLayout typeBtnsLayout;
    private StartAppPresenter presenter;
    private AbleToChangeFragment ableToChangeFragment;

    public static FragmentStartApp newInstance() {
        FragmentStartApp fragment = new FragmentStartApp();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new StartAppPresenter(this);
        ableToChangeFragment = (AbleToChangeFragment) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start_app, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.sign_in_btn)
    public void signInBtnClick() {
        presenter.signInChoose();
    }

    @OnClick(R.id.sign_up_btn)
    public void signUpBtnClick() {
        presenter.signUpChoose();
    }

    @OnClick(R.id.as_worker)
    public void asWorkerBtnClick() {
        presenter.asWorkerChoose();
    }

    @OnClick(R.id.as_leader)
    public void asLeaderBtnClick() {
        presenter.asLeaderChoose();
    }

    @Override
    public void setLogBtnInvisible(boolean isInvisible) {
        if (isInvisible) {
            logBtnsLayout.setVisibility(View.GONE);
        } else logBtnsLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTypeBtnInvisible(boolean isInvisible) {
        if (isInvisible) {
            typeBtnsLayout.setVisibility(View.GONE);
        } else typeBtnsLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoginDialog() {
        DialogLogIn dialogLogin = new DialogLogIn();
        dialogLogin.show(getActivity().getSupportFragmentManager(), "addCard");
    }

    @Override
    public void toRegistration() {
        ableToChangeFragment.changeFragments(R.id.main_container, FragmentRegistration.newInstance(), FragmentRegistration.TAG, true);
    }

    @Override
    public void onBackPressed() {
        if (typeBtnsLayout.getVisibility() == View.VISIBLE) {
            setTypeBtnInvisible(true);
            setLogBtnInvisible(false);
        } else {
            getActivity().finish();
        }
    }
}
