package team.gu.com.workingtimecontrol.regOrAuthModule.view;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import team.gu.com.workingtimecontrol.R;
import team.gu.com.workingtimecontrol.appBodyModule.AppBodyActivity;
import team.gu.com.workingtimecontrol.regOrAuthModule.AbleToChangeFragment;
import team.gu.com.workingtimecontrol.regOrAuthModule.presenter.PresenterAuthorisation;
import team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface.AuthorisationInterfaceView;
import team.gu.com.workingtimecontrol.security.SharedPreferenceWorker;

/**
 * Фрагмент авторизации, сюда пользователь будит попадать при входе в приложение если
 * установлен пин-код
 */
public class FragmentAuthorisation extends Fragment implements AuthorisationInterfaceView {
    public static final String TAG = "Авторизация";
    EditText logInData;
    TextInputLayout logInDataUserLayout;
    EditText logInPass;
    TextInputLayout logInPassLayout;
    EditText repairPassLogin;
    EditText repairPassNewPassword;
    TextInputLayout repairPaassLoginLayout;
    TextInputLayout repairPaassNewPasswordLayout;
    TextView forgotPass;
    LinearLayout showloginDataLayout;
    LinearLayout forgotPassLayout;
    Button loginBtn;
    Button cancelDialog;

    private AbleToChangeFragment callBackFragment;

    private PresenterAuthorisation presenter;

    /**
     * Получение экземпляра фрагмента
     */
    public static FragmentAuthorisation newInstance() {
        FragmentAuthorisation fragment = new FragmentAuthorisation();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBackFragment = (AbleToChangeFragment) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_authentification, container, false);
        presenter = new PresenterAuthorisation(this);
        initViews(view);
        initListeners();
        return view;
    }

    private void initListeners() {
        forgotPass.setOnClickListener(v -> {
            presenter.forgotPass();
        });
        loginBtn.setOnClickListener(v -> {
            if (!presenter.isUserForgotPassword())
                presenter.authorisation(logInData.getText().toString(), logInPass.getText().toString());
            else
                presenter.resetPassword(repairPassLogin.getText().toString(), repairPassNewPassword.getText().toString());
        });
        cancelDialog.setOnClickListener(v -> getActivity().finish());
    }

    private void initViews(View view) {
        logInData = view.findViewById(R.id.log_in_data_edit_text);
        logInDataUserLayout = view.findViewById(R.id.log_in_data_il);
        logInPass = view.findViewById(R.id.log_in_password_edit_text);
        logInPassLayout = view.findViewById(R.id.log_in_password_il);
        repairPassLogin = view.findViewById(R.id.repair_password_login);
        repairPaassLoginLayout = view.findViewById(R.id.repair_password_login_il);
        forgotPass = view.findViewById(R.id.forgot_pass);
        showloginDataLayout = view.findViewById(R.id.login_password_layout);
        forgotPassLayout = view.findViewById(R.id.forgot_password_layout);
        loginBtn = view.findViewById(R.id.log_in_btn);
        cancelDialog = view.findViewById(R.id.cancel_dialog_auth_btn);
        repairPassNewPassword = view.findViewById(R.id.repair_new_password_et);
        repairPaassNewPasswordLayout = view.findViewById(R.id.repair_new_password_il);
        forgotPassLayout.setVisibility(View.GONE);
        String login = SharedPreferenceWorker.getInstance().getUserLogin();
        logInData.setText(login);
    }

    /**
     * Переход в тело приложения
     */
    @Override
    public void startMainActivityAndClearOldData() {
        Intent intent = new Intent(getContext(), AppBodyActivity.class);
        startActivity(intent);
        callBackFragment.removeFragment(this);
        getActivity().finish();
    }

    @Override
    public void setRepairPassLoginInputError(String error) {
        repairPaassLoginLayout.setError(error);
    }

    @Override
    public void setRepairPassNewPasswordInputError(String error) {
        repairPaassNewPasswordLayout.setError(error);
    }

    @Override
    public void setAuthError(String error) {
        logInPassLayout.setError(error);
    }

    @Override
    public void forgotPassBtnClick(boolean isForgotPass) {
        /**пользователь забыл пароль*/
        if (isForgotPass) {
            showloginDataLayout.setVisibility(View.GONE);
            forgotPassLayout.setVisibility(View.VISIBLE);
            forgotPass.setTextColor(Color.BLUE);
        } else {
            showloginDataLayout.setVisibility(View.VISIBLE);
            forgotPassLayout.setVisibility(View.GONE);
            forgotPass.setTextColor(Color.BLACK);
        }
    }

    @Override
    public void setLoginError(String result) {
        logInDataUserLayout.setError(result);
    }

    @Override
    public void setPasswordError(String result) {
        logInPassLayout.setError(result);
    }

}
