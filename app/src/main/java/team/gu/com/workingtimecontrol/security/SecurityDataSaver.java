package team.gu.com.workingtimecontrol.security;

/**
 * Интерфейс для SharepPreferenceWorker? будет использоваться в presenterax
 */
public interface SecurityDataSaver {

    boolean checkAuthorised();

    void saveAuthState(boolean isAuthorised);

    void savePinCodeExistState(boolean isExist);

    boolean checkPinCodeExisting();

    void saveChiper(String chiper);

    String getUserChiper();

    void saveUserLogin(String login);

    String getUserLogin();

    boolean checkWorkerState();

    void saveWorkerState(boolean isWorker);

    boolean checkLeaderState();

    void saveLeaderState(boolean isLeader);

    void saveUserPassword(String password);

    String getUserPassword();
}
