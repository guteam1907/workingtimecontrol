package team.gu.com.workingtimecontrol.entity;

public class LeaderUser {
    public static final String key = "LEADER_KEY";
    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
