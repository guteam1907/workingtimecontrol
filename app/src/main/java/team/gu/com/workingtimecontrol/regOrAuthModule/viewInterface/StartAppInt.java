package team.gu.com.workingtimecontrol.regOrAuthModule.viewInterface;

public interface StartAppInt {
    void setLogBtnInvisible(boolean isInvisible);

    void setTypeBtnInvisible(boolean isInvisible);

    void showLoginDialog();

    void toRegistration();
}
